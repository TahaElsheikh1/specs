
Pod::Spec.new do |s|
    s.name             = 'VodafoneThirdParty'
    s.version = "0.4.6"
    s.summary = "VodafoneThirdParty handle AnaVodafone thirdParty"
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    TODO: Add long description of the pod here.
    DESC
    
    s.homepage         = 'https://github.com/VodafoneEgypt/VodafoneThirdParty'
    # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author = { "TahaElsheikh1" => "taha.abdelraouf-kotb@vodafone.com" }
    s.source = { :git => "https://gitlab.com/TahaElsheikh1/vodafonethirdparty.git",
        :tag => "#{s.version}" }
    
    s.ios.deployment_target = '9.0'
    
    s.source_files = 'VodafoneThirdParty/Classes/**/*'
    
    # s.resource_bundles = {
    #   'VodafoneThirdParty' => ['VodafoneThirdParty/Assets/*.png']
    # }
    
    s.resources = "VodafoneThirdParty/**/*.{xib,a}"
    
    s.dependency 'APAddressBook'
    s.dependency 'Languagehandlerpod'
end
