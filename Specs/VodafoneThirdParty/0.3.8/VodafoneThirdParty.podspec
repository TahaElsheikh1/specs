Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '12.0'
s.name = "VodafoneThirdParty"
s.summary = "VodafoneThirdParty handle AnaVodafone thirdParty"
s.requires_arc = true

# 2
s.version = "0.3.8"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "TahaElsheikh1" => "taha.abdelraouf-kotb@vodafone.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/TahaElsheikh1/vodafonethirdparty"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://gitlab.com/TahaElsheikh1/vodafonethirdparty.git",
:tag => "#{s.version}" }

# 7
s.framework = "UIKit"

# 8
s.source_files = "VodafoneThirdParty/**/*.{h,m}"

# 9
#s.resources = "VodafoneThirdParty/**/*.{png,jpeg,jpg,storyboard,xib,xcassets,json,a}"
  
#s.dependency 'APAddressBook'

s.dependency 'Languagehandlerpod'

end
